package sorting;

public class Finder {


    /**
     * Some searching algorithm that possibly the slowest algorithm.
     * This algorithm can search a value irregardless of whether the sequence already sorted or not.
     * @param arrOfInt is a sequence of integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */
    public static int slowSearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;

        for (int element : arrOfInt) {
            if (element == searchedValue) {
                returnValue = element;
            }
        }

        return returnValue;
    }
    
    public static int fastSearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;

        for (int i = 0; i < arrOfInt.length / 2; i++) {
            if (arrOfInt[i] == searchedValue) {
                returnValue = arrOfInt[i];
                break;
            } else if (arrOfInt[arrOfInt.length - 1 - i] == searchedValue) {
                returnValue = arrOfInt[arrOfInt.length - 1 - i];
                break;
            }
        }

        return returnValue;
    }
    
    /**
     * For sorted array.
     */
    public static int binarySearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;
        int start = 0;
        int end = arrOfInt.length - 1;

        while (start <= end) {
            int mid = (start + end) / 2;
            if (searchedValue == arrOfInt[mid]) {
                return mid;
            }
            if (searchedValue < arrOfInt[mid]) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }

        return returnValue;
    }

}
