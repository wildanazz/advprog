package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    public static int[] quickSort(int[] numbersArr, int lowerIndex, int higherIndex) {
        int i = lowerIndex;
        int j = higherIndex;
        // Taking pivot as middle index number
        int pivot = numbersArr[(lowerIndex + higherIndex) / 2];
        // Divide into two arrays
        while (i <= j) {
            /**
             * In each iteration, we will identify a number from left side which 
             * is greater then the pivot value, and also we will identify a number 
             * from right side which is less then the pivot value. Once the search 
             * is done, then we exchange both numbers.
             */
            while (numbersArr[i] < pivot) {
                i++;
            }

            while (numbersArr[j] > pivot) {
                j--;
            }

            if (i <= j) {
                int temp = numbersArr[i];
                numbersArr[i] = numbersArr[j];
                numbersArr[j] = temp;
                // Move index to next position on both sides
                i++;
                j--;
            }
        }
        // Call quickSort() method recursively
        if (lowerIndex < j) {
            quickSort(numbersArr, lowerIndex, j);
        }
        if (i < higherIndex) {
            quickSort(numbersArr, i, higherIndex);
        }
        return numbersArr;
    }

}
