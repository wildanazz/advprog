package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.EmmentalCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.WhiteClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.LeanDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.BarbecueSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BrownOnion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Celery;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Tomato;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new LeanDough();
    }

    public Sauce createSauce() {
        return new BarbecueSauce();
    }

    public Cheese createCheese() {
        return new EmmentalCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Tomato(), new Celery(), new BrownOnion(), new BlackPepper()};
        return veggies;
    }

    public Clams createClam() {
        return new WhiteClams();
    }
}