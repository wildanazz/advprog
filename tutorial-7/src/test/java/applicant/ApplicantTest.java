package applicant;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;

public class ApplicantTest {
    private static final Applicant applicant = new Applicant();
    private static Predicate<Applicant> qualifiedEvaluator;
    private static Predicate<Applicant> creditEvaluator;
    private static Predicate<Applicant> employmentEvaluator;
    private static Predicate<Applicant> criminalRecordsEvaluator;

    @Before
    public void setUp() {
        qualifiedEvaluator = Applicant::isCredible;
        creditEvaluator = anApplicant -> anApplicant.getCreditScore() > 600;
        employmentEvaluator = anApplicant -> anApplicant.getEmploymentYears() > 0;
        criminalRecordsEvaluator = anApplicant -> !anApplicant.hasCriminalRecord();
    }

    @Test
    public void testEvaluatorEvaluateApplicantCorrectly() {
        assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator.and(creditEvaluator)));
        assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator.and(employmentEvaluator)));
        assertFalse(Applicant.evaluate(applicant, qualifiedEvaluator.and(employmentEvaluator)
                .and(criminalRecordsEvaluator)));
        assertFalse(Applicant.evaluate(applicant, qualifiedEvaluator.and(employmentEvaluator)
                .and(creditEvaluator).and(criminalRecordsEvaluator)));
    }
}
