import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {
    
    private static final Map<String, Integer> scores = new HashMap<>();
    
    @Before
    public void setUp() {
        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);
    }

    @Test
    public void testGroupingPersonExistNumberOfPersonCorrect() {
        assertEquals(2, ScoreGrouping.groupByScores(scores).get(11).size());
        assertEquals(1, ScoreGrouping.groupByScores(scores).get(12).size());
        assertEquals(3, ScoreGrouping.groupByScores(scores).get(15).size());
    }

    @Test
    public void testGroupingPersonExistNumberOfPersonIncorrect() {
        assertFalse(ScoreGrouping.groupByScores(scores).get(11).size() == 1);
        assertFalse(ScoreGrouping.groupByScores(scores).get(12).size() == 0);
        assertFalse(ScoreGrouping.groupByScores(scores).get(15).size() == 2);
    }
    
}