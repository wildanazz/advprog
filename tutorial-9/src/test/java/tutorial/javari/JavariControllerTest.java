package tutorial.javari;

import org.springframework.web.client.RestTemplate;
import tutorial.javari.animal.*;

import java.util.LinkedHashMap;
import java.util.List;

public class JavariControllerTest {

    public static final String REST_SERVICE_URI = "http://localhost:8080/SpringBootRestApi/api";

    @SuppressWarnings("unchecked")
    private static void listAllAnimals(){
        System.out.println("Testing listAllAnimals API-----------");

        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> animalsMap = restTemplate.getForObject(REST_SERVICE_URI+"/animal/", List.class);

        if(animalsMap!=null){
            for(LinkedHashMap<String, Object> map : animalsMap){
                System.out.println("Animal : id="+map.get("id")+", Name="+map.get("name")+", Type="+map.get("type")+", Body ="+map.get("body")+", Condition ="+map.get("condition"));;
            }
        }else{
            System.out.println("No animal exist----------");
        }
    }

    private static void getAnimal(){
        System.out.println("Testing getAnimal API----------");
        RestTemplate restTemplate = new RestTemplate();
        Animal animal = restTemplate.getForObject(REST_SERVICE_URI+"/animal/1", Animal.class);
        System.out.println(animal);
    }

    public static void main(String args[]){
        listAllAnimals();
        getAnimal();
    }

}