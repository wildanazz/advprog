package tutorial.javari.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

import org.springframework.stereotype.Service;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

@Service("animalService")
public class AnimalService {

    private static List<Animal> animals;

    static{
        animals = populateDummyUsers();
    }

    public List<Animal> findAllAnimals() {
        return animals;
    }

    public Animal findById(Integer id) {
        for(Animal animal : animals){
            if(animal.getId() == id){
                return animal;
            }
        }
        return null;
    }

    public void deleteById(Integer id) {
        for (Iterator<Animal> iterator = animals.iterator(); iterator.hasNext(); ) {
            Animal animal = iterator.next();
            if (animal.getId() == id) {
                iterator.remove();
            }
        }
    }

    public void saveAnimal(Animal animal) {
        animals.add(animal);
    }

    private static List<Animal> populateDummyUsers(){
        List<Animal> animals = new ArrayList<Animal>();
        animals.add(new Animal(1,"Mamalia","Kuda", Gender.MALE, 5,130, Condition.HEALTHY));
        animals.add(new Animal(2,"Mamalia","Sapi", Gender.MALE, 4, 150, Condition.SICK));
        animals.add(new Animal(3,"Mamalia","Domba", Gender.MALE, 4, 120, Condition.SICK));
        animals.add(new Animal(4,"Mamalia","Monyet", Gender.FEMALE, 2, 110, Condition.HEALTHY));
        return animals;
    }
}
