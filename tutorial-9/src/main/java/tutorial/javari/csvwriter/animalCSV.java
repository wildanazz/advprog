package tutorial.javari.csvwriter;

import tutorial.javari.animal.Animal;
import tutorial.javari.service.AnimalService;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class animalCSV {

    public static void updateAnimalCsv(AnimalService animalService){
        try {
            String csvFile = "data.csv";
            FileWriter writer = new FileWriter(csvFile);

            List<Animal> lists = animalService.findAllAnimals();

            for (Animal animal : lists) {
                List<String> list = new ArrayList<>();
                list.add(animal.getId().toString());
                list.add(animal.getType());
                list.add(animal.getName());
                list.add(animal.getGender().toString());
                list.add(Double.toString(animal.getLength()));
                list.add(Double.toString(animal.getWeight()));
                list.add(animal.getCondition().toString());

                CSVwriter.writeLine(writer, list);
            }
            writer.flush();
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}