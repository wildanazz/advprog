package tutorial.javari;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.util.UriComponentsBuilder;
import tutorial.javari.animal.*;
import tutorial.javari.service.*;
import tutorial.javari.util.*;
import tutorial.javari.csvwriter.*;

import java.util.List;
import java.util.Map;

@RestController
public class JavariController {

    @Autowired
    AnimalService animalService;

    //  GET ALL THE ANIMALS
    @RequestMapping(value = "/javari", method = RequestMethod.GET)
    public ResponseEntity<List<Animal>> listAllAnimals() {
        List<Animal> animals = animalService.findAllAnimals();
        if (animals.isEmpty()) {
            return new ResponseEntity(new CustomErrorType("There are no animals"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<Animal>>(animals, HttpStatus.OK);
    }

    //  GET ANIMAL BY ID
    @RequestMapping(value = "/javari/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getAnimal(@PathVariable("id") Integer id) {
        Animal animal = animalService.findById(id);
        if (animal == null) {
            return new ResponseEntity(new CustomErrorType("Animal with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Animal>(animal, HttpStatus.OK);
    }

    //  DELETE ANIMAL BY ID
    @RequestMapping(value = "/javari/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteAnimal(@PathVariable("id") Integer id) {
        Animal animal = animalService.findById(id);
        if (animal == null) {
            return new ResponseEntity(new CustomErrorType("Unable to delete. Animal with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        animalService.deleteById(id);
        animalCSV.updateAnimalCsv(animalService);
        return new ResponseEntity<Animal>(HttpStatus.NO_CONTENT);
    }

    //  ADD NEW ANIMAL
    @RequestMapping(value = "/javari", method = RequestMethod.POST)
    public ResponseEntity<?> createUser(@RequestBody Map<String, String> animalAttribute, UriComponentsBuilder ucBuilder) {
        System.out.println(animalAttribute.get("id"));
        int id = Integer.parseInt(animalAttribute.get("id"));
        String type = animalAttribute.get("type");
        String name = animalAttribute.get("name");
        Gender gender = Gender.parseGender(animalAttribute.get("gender"));
        double length = Double.parseDouble(animalAttribute.get("length"));
        double weight = Double.parseDouble(animalAttribute.get("weight"));
        Condition condition = Condition.parseCondition(animalAttribute.get("condition"));

        Animal animal = new Animal(id, type, name, gender, length, weight, condition);
        animalService.saveAnimal(animal);
        animalCSV.updateAnimalCsv(animalService);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/javari/{id}").buildAndExpand(animal.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }
}